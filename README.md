# Ansible role for PostgreSQL installation

## Introduction

[PostgreSQL](https://www.postgresql.org/) is an SQL database.

This role installs the server and initializes a default cluster.
On Debian systems this role only uses the 'main' default cluster.

This role manage the configuration file with defaults which should be
suitable for most users. If you need to customize, then use the
`conf.d` subdirectory to drop configuration file bits (PostgreSQL >=9.3
required), The firewall configuration is left to your care.

## Variables

- **listen_addresses**: list of IP/DNS names to listen on (defaults to
                        localhost)
- **service_profile**: service "sizing" depending on expected load,
                       value in low/medium/high (defaults to low)
- **backup_databases**: list of databases names to backup daily
- **with_tls**: enable TLS support:
  + a DH file is generated (path can be changed with `dh_file`)
  + `tls.crt` and `tls.key` certificate files are expected in the
    configuration directory (path can be changed with `cert_file` and
    `key_file)
  + connections from outside are allowed if secure
- **manage_pg_hba**: install a simple PostgreSQL client authentication configuration file allowing local access and Zabbix user for monitoring

